package model.logic;

import java.io.FileReader;

import com.opencsv.CSVReader;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo<T> {

	/**
	 * Atributos del modelo del mundo
	 */
	private IQueue<Node<UberData>> dataQueue;
	private IStack<T> dataStack;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		dataStack = (IStack<T>) new Stack<T>();
		dataQueue = (IQueue<Node<UberData>>) new Queue<Node<UberData>>();
	}

	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int getStackSize()
	{
		return dataStack.size();
	}

	public int getQueueSize()
	{
		return dataQueue.darTamanio();
	}

	//TODO Metodo de cargar CSV.
	@SuppressWarnings("unchecked")
	public int cargarCSV(String pRutaArchivo)
	{
		int contadorLineas = 0;
		try {
			//Reader que lee el csv linea por l�nea
			CSVReader reader = new CSVReader(new FileReader(pRutaArchivo));
			//retorna un arreglo de arreglos de strings
			for(String[] linea : reader) 
			{

				if(contadorLineas != 0)
				{	
					int pSourceid = Integer.parseInt(linea[0]);
					int pDstid = Integer.parseInt(linea[1]);
					int pHod = Integer.parseInt(linea[2]);
					double pMean_travel_time = Double.parseDouble(linea[3]);
					double pStandard_deviation_travel_time = Double.parseDouble(linea[4]);
					double pGeometric_mean_travel_time = Double.parseDouble(linea[5]);
					double pGeometric_standard_deviation_travel_time = Double.parseDouble(linea[6]);

					//con cada linea leida se crea un objeto de tipo UberData
					UberData dato = new UberData(pSourceid, pDstid, 
							pHod, pMean_travel_time, pStandard_deviation_travel_time,
							pGeometric_mean_travel_time,
							pGeometric_standard_deviation_travel_time);
					//se crea un nodo con el objeto anterior dentro.
					Node<UberData> node = new Node<UberData>(dato);
					//Push a la pila
					dataStack.push((Node<T>) node);
					//enqueue e
					((Queue<Node<UberData>>) dataQueue).ponerEnCola(node);
				}
				contadorLineas++;
			}
			reader.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
		return contadorLineas;
	}

	public Queue<Node<UberData>> darClustersConsecutivosHora(int horaInicial) throws Exception 
	{
		Queue<Node<UberData>> masGrande = new Queue<Node<UberData>>();
		Node<UberData> n = (Node<UberData>)dataQueue.sacarDeCola();
		while( n != null)
		{	
			int hora = ((UberData)n.getObject()).getHod();
			if(hora >= horaInicial)  //Identifica el primero de un cluster
			{
				Queue<Node<UberData>> cola = (Queue<Node<UberData>>)new Queue<Node<UberData>>();
				cola.ponerEnCola(n);
				Node<UberData> siguiente = (Node<UberData>)dataQueue.sacarDeCola();
				int horaDelSiguiente = ((UberData)siguiente.getObject()).getHod();
				while ( horaDelSiguiente >= hora )
				{
					cola.ponerEnCola(n);
					siguiente = (Node<UberData>)dataQueue.sacarDeCola();
					horaDelSiguiente = ((UberData)siguiente.getObject()).getHod();
				} //Termina de armar el cluster
				//Compara con el cluster mas grande
				if (cola.darTamanio() > masGrande.darTamanio())
					masGrande = cola;
			}	
			n = (Node<UberData>)dataQueue.sacarDeCola();
		}
		return masGrande;
	}

	// consultar ultimos viajes segun la hora ingresada por par�metro

	public Queue<Node<UberData>> ultimosViajesHora(int hora , int cantidad)
	{
		Queue<Node<UberData>> cola = new Queue<Node<UberData>>();
		int viajesContados = 0;
		UberData dato = (UberData)dataStack.pop();
		if(hora <= 24 && cantidad <= getStackSize())
		{
			while( !dataStack.isEmpty() && dato !=null && viajesContados < cantidad)
			{
				if(dato !=null )
				{
					if(dato.getHod() == hora)
					{
						cola.ponerEnCola(new Node<UberData>(dato));
						viajesContados++;
					}
				}
				dato = (UberData)dataStack.pop();
			}
		}
		return cola;
	}

	public T getFirst()
	{
		return dataStack.getFirst(); 
	}

	public UberData darPrimer(){
		return dataQueue.darPrimero().getObject();
	}
}
