package model.logic;

public class UberData {

	//sourceid,
	//dstid,
	//hod,
	//mean_travel_time,
	//standard_deviation_travel_time,
	//geometric_mean_travel_time,
	//geometric_standard_deviation_travel_time
	
	private int sourceid;
	private int dstid;
	private int hod;
	private double mean_travel_time;
	private double standard_deviation_travel_time;
	private double geometric_mean_travel_time;
	private double geometric_standard_deviation_travel_time;

	public UberData(int pSourceid,int pDstid,int pHod,double pMean_travel_time,double pStandard_deviation_travel_time,double pGeometric_mean_travel_time,double pGeometric_standard_deviation_travel_time)
	{
		sourceid = pSourceid;
		dstid = pDstid;
		hod = pHod;
		mean_travel_time = pMean_travel_time;
		standard_deviation_travel_time = pStandard_deviation_travel_time;
		geometric_mean_travel_time = pGeometric_mean_travel_time;
		geometric_standard_deviation_travel_time = pGeometric_standard_deviation_travel_time;
	}

	public int getSourceid() {
		return sourceid;
	}

	public int getDstid() {
		return dstid;
	}

	public int getHod() {
		return hod;
	}

	public double getMean_travel_time() {
		return mean_travel_time;
	}

	public double getStandard_deviation_travel_time() {
		return standard_deviation_travel_time;
	}

	public double getGeometric_mean_travel_time() {
		return geometric_mean_travel_time;
	}

	public double getGeometric_standard_deviation_travel_time() {
		return geometric_standard_deviation_travel_time;
	}
}