package controller;

import java.util.Scanner;

import model.data_structures.Node;
import model.data_structures.Queue;
import model.logic.MVCModelo;
import model.logic.UberData;
import view.MVCView;

public class Controller {
	
	/* Instancia del Modelo*/
	private MVCModelo<UberData> modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo<UberData>();
	}

	public void run()   
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("Cargar Archivo del primer trimestre de 2018");
				modelo = new MVCModelo<UberData>(); 
				//    "./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"
				int lineas = modelo.cargarCSV("./data/test.csv");
				System.out.println("Total de datos cargados: "+lineas);

				UberData primerViaje = (UberData) modelo.getFirst();
				UberData ultimoViaje = (UberData) modelo.darPrimer();
				System.out.println("Origen del primer viaje: "+primerViaje.getSourceid());
				System.out.println("Destino del primer viaje: "+primerViaje.getDstid());
				System.out.println("Hora del primer viaje: "+primerViaje.getHod());
				System.out.println("Tiempo promedio del primer Viaje: "+primerViaje.getMean_travel_time());
				System.out.println("----------------------------------------------------------------------------------");
				System.out.println("Origen del ultimo viaje: "+ultimoViaje.getSourceid());
				System.out.println("Destino del ultimo viaje: "+ultimoViaje.getDstid());
				System.out.println("Hora del ultimo viaje: "+ultimoViaje.getHod());
				System.out.println("Tiempo promedio del ultimo Viaje: "+ultimoViaje.getMean_travel_time());
				break;

			case 2:
				System.out.println("Procesar cola para consultar el grupo ordenado m�s grande");
				System.out.println("Ingrese la hora (en formato de 24 horas) a buscar");
				int horaInicial = lector.nextInt();
				try {
					Queue<Node<UberData>> resultados = modelo.darClustersConsecutivosHora(horaInicial);
					for (int i = 0; i < resultados.darTamanio(); i++)
					{
						UberData viaje = resultados.sacarDeCola().getObject();
						System.out.println("------------------------------------------------------------------");
						System.out.println("Viaje "+(i+1)+":");
						System.out.println("Hora: "+viaje.getHod());
						System.out.println("Zona de origen: "+viaje.getSourceid());
						System.out.println("Zona de destino: "+viaje.getDstid());
						System.out.println("TiempoPromedio: "+viaje.getMean_travel_time());
						System.out.println("-------------------------------------------------------------------");					
					}
				} catch (Exception e) {e.printStackTrace();}
				break;

			case 3:
				System.out.println("Procesar pila para buscar �ltimos viajes");
				System.out.println("Ingrese la hora (en formato de 24 horas) a buscar");
				int hora = lector.nextInt();
				System.out.println("Ingrese la cantidad de viajes a buscar");
				int cantidad = lector.nextInt();
				
				//Yo implementé la cola con un 
				//generics que extiende a Node con T: UberData
				// es decir, Queue<Node<UberData>> so lo que está en las colas son nodos
				try
				{
				Queue<Node<UberData>> resultado = modelo.ultimosViajesHora(hora, cantidad);				
				System.out.println("Resultado de la consulta: ");
				
				for(int i = 0; i < resultado.darTamanio();i++)
				{
					int horadelDia = resultado.sacarDeCola().getObject().getHod();
					int zonaOrigen = resultado.sacarDeCola().getObject().getSourceid();
					int zonaDestino = resultado.sacarDeCola().getObject().getDstid();
					double tiempoPromedio = resultado.sacarDeCola().getObject().getMean_travel_time();
					int indice = i+1;
					System.out.println("------------------------------------------------------------------");
					System.out.println("Viaje "+indice+":");
					System.out.println("Hora: "+horadelDia);
					System.out.println("Zona de origen: "+zonaOrigen);
					System.out.println("Zona de destino: "+zonaDestino);
					System.out.println("TiempoPromedio: "+tiempoPromedio);
					System.out.println("-------------------------------------------------------------------");
				}
				} catch(Exception e) {e.printStackTrace();}
				break;

			case 4:
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;

			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
