package test.logic;

import static org.junit.Assert.*;
import model.logic.MVCModelo;
import model.logic.UberData;

import org.junit.Before;
import org.junit.Test;

public class TestMVCModelo {

	private MVCModelo<UberData> modelo;

	@Before
	public void setUp1() {
		modelo= new MVCModelo<UberData>();
	}

	@Test
	public void setUp2()
	{
		modelo.cargarCSV(".data/Test.csv");
	}
	// Pruebas empiezan Aqu�

	@Test
	public void testMVCModelo() {
		assertTrue(modelo!=null);
		assertEquals(0, modelo.getQueueSize());  // Modelo con 0 elementos presentes.
		assertEquals(0, modelo.getStackSize());  // Modelo con 0 elementos presentes.
	}

	@Test
	public void testDarTamano() {
		assertEquals(0, modelo.getQueueSize());  // Modelo con 0 elementos presentes.
		assertEquals(0, modelo.getStackSize());  // Modelo con 0 elementos presentes.
	}

	@Test
	public void testCargarCSV() {
		setUp2();
		assertTrue(modelo.getQueueSize() > 0);	
		assertTrue(modelo.getStackSize() > 0);
	}

	@Test
	public void testDarClustersConsecutivosHora()
	{
		int i = 0;
		while(i<=24)
		{
			setUp2();
			try {
				assertNotNull(modelo.darClustersConsecutivosHora(i));
			} 
			catch (Exception e)
			{
				if(e.getMessage().startsWith("La lista"));
				{
					assertTrue(modelo.getQueueSize() == 0);
				}
			}
			finally 
			{
				i++;
			}
		}

	}

	@Test
	public void testUltimosViajesHora()
	{
		int i = 0;
		while(i<=24)
		{
			setUp2();
			assertNotNull(modelo.ultimosViajesHora(i,1));
			assertTrue(modelo.getStackSize() == 0);
			i++;
		}
	}
}
