package test.logic;

import model.logic.UberData;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestUberData {

	private UberData uberData;

	@Before
	public void setUp1()
	{
		uberData = new UberData(1, 2, 3, 4, 5, 6, 7);	
	}

	@Test
	public void testUberDdata()
	{
		assertNotNull(uberData);
	}
	@Test
	public void testGetSourceid() {
		assertTrue(uberData.getSourceid() == 1);
	}
	@Test
	public void testGetDstid() {
		assertTrue(uberData.getDstid() == 2);
	}
	@Test
	public void testGetHod() {
		assertTrue(uberData.getHod() == 3);
	}
	@Test
	public void testGetMean_travel_time() {
		assertTrue(uberData.getMean_travel_time() == 4);
	}
	@Test
	public void testGetStandard_deviation_travel_time() {
		assertTrue(uberData.getStandard_deviation_travel_time() == 5);
	}
	@Test
	public void testGetGeometric_mean_travel_time() {
		assertTrue(uberData.getGeometric_mean_travel_time() == 6);
	}
	@Test
	public void testGetGeometric_standard_deviation_travel_time() {
		assertTrue(uberData.getGeometric_standard_deviation_travel_time() == 7);	
	}
}